/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.com.protec.dbconfigFileParser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.InvalidPropertiesFormatException;
import java.util.Map;
import java.util.Properties;

/**
 * 
 * @author Jerbi
 */

/*
 * XmlDbParameters : Intéraction avec le fichier xml contenant les paramètres
 * de connexion aux tables de base de données externe. Respnsabilités : 1-
 * Récupération des valeurs enregistrés dans le fichier xml. la méthode est
 * : getDBProperties. 2- Envoi des valeurs aux classes pour mapper les relations
 * entre les Objets et les Tables du base de données
 */

public class XmlDbTablesParameters {

	public static Properties getTablePropertieValue(String filepath)
			throws InvalidPropertiesFormatException, FileNotFoundException {
		
		
		Properties props = new Properties();
		InputStream is = new FileInputStream(filepath);
		// load the xml file into properties format
		try {

			props.loadFromXML(is);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return props;
	}
}
