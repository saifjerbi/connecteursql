/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.com.protec.abstractClass;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Jerbi
 */
public abstract class AbstractFacade<T> {

    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    /**
     * insert
     *
     * @param entity
     * @throws Exception
     */
    public void insert(T bean) throws Exception {

        Session session = HibernateUtil.currentSession();
        Transaction tx = session.beginTransaction();
        try {

            session.save(bean);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            System.out.println(ex);
        } finally {
            HibernateUtil.closeSession();
        }
    }

    /**
     * update
     *
     * @param bean
     * @throws Exception
     */
    public void update(T bean) throws Exception {
        Session session = HibernateUtil.currentSession();
        Transaction tx = session.beginTransaction();
        try {

            session.update(bean);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            System.out.println(ex);
        } finally {
            HibernateUtil.closeSession();
        }

    }

    public void delete(T bean) throws Exception {

        Session session = HibernateUtil.currentSession();
        Transaction tx = session.beginTransaction();
        try {

            session.delete(bean);
            tx.commit();
        } catch (Exception ex) {
            tx.rollback();
            System.out.println(ex);
        } finally {
            HibernateUtil.closeSession();
        }


    }

    /**
     * findAll
     *
     * @return
     * @throws Exception
     */
    public List<T> findAll() throws Exception {

        Session session = null;
        try {
            session = HibernateUtil.currentSession();
            Query query = session.createQuery("select p from " + this.entityClass.getName() + " as p ");
            List temp = query.list();
            //ProduitBean ret[] = new ProduitBean[temp.size()];
            //temp.toArray(ret);

            System.out.println("Fetched " + temp.size() + " rows.");
            return temp;

        } catch (Exception ex) {
            ex.printStackTrace();
            HibernateUtil.closeSession();
            return null;
        } finally {
            HibernateUtil.closeSession();
        }


    }

    /**
     * findByPrimaryKey
     *
     * @param IdProduit
     * @return
     * @throws Exception
     */
    public T findByPrimaryKey(int idProd) throws Exception {
        Session session = null;
        try {
            session = HibernateUtil.currentSession();
            Query query = session.createQuery("select p from " + this.entityClass.getName() + " as p where p.id=:id");
            query.setLong("id", idProd);
            T produit = (T) query.uniqueResult();
            return produit;

        } catch (Exception ex) {
            ex.printStackTrace();
            HibernateUtil.closeSession();
            return null;
        } finally {
            HibernateUtil.closeSession();
        }

    }
}
