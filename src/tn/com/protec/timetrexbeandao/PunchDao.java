/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.com.protec.timetrexbeandao;

import java.util.List;
import tn.com.protec.timetrexbean.Punch;

/**
 *
 * @author Jerbi
 */
public interface PunchDao {
        /**
	 * Insérer une nouvelle Ligne dans la table User.
	 * @param bean
     * @throws Exception
	 */
	public void insert(Punch bean) throws Exception;
	/**
	 * Mettre à jour une ligne dans la table User.
	 * @param bean
     * @param IdUser
     * @throws Exception
     */
    public void update(Punch bean) throws Exception;
	/**
	 * Supprimer une ligne de la table User.
	 * @param IdUser
     * @throws Exception
	 */
    public void delete(Punch bean) throws Exception;
	/**
	 * Recherche par Clé Primaire
	 */
    public Punch findByPrimaryKey(int idPunch) throws Exception ;
    
	/**
     * findAll
     * @return
     * @throws Exception
     */
    public List<Punch>findAll() throws Exception;
    
    /*
     * 
     * Get Punch from external DataBase 
     * To TimeTrex Data Base
     * 
     */
    public List<Punch> findAllfromExternalDataBaseServerTable() throws Exception;
    /*
     * 
     * Import Punch to PostgreSQL
     * TimeTrex Data Base
     * */
    public void importPunchToTimeTrexDataBase(Punch[] punch)throws Exception;
}
