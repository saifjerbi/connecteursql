/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.com.protec.timetrexbeandao;

import java.util.List;
import tn.com.protec.timetrexbean.PunchControl;

/**
 *
 * @author protec
 */
public interface PunchControlDao {
   /**
	 * Insérer une nouvelle Ligne dans la table User.
	 * @param bean
     * @throws Exception
	 */
	public void insert(PunchControl bean) throws Exception;
	/**
	 * Mettre à jour une ligne dans la table User.
	 * @param bean
     * @param IdPunchControl
     * @throws Exception
     */
    public void update(PunchControl bean) throws Exception;
	/**
	 * Supprimer une ligne de la table User.
	 * @param IdPunchControl
     * @throws Exception
	 */
    public void delete(PunchControl bean) throws Exception;
	/**
	 * Recherche par Clé Primaire
	 */
    public PunchControl findByPrimaryKey(int idPunchControl) throws Exception ;
    
	/**
     * findAll
     * @return
     * @throws Exception
     */
    public List<PunchControl>findAll() throws Exception;
    /*
     * 
     * Get PunchControl from external DataBase 
     * To TimeTrex Data Base
     * 
     */
    public List<PunchControl> findAllfromExternalDataBaseServerTable(String dataBase, String table) throws Exception;
    /*
     * 
     * Import PunchControl to PostgreSQL
     * TimeTrex Data Base
     * */
    public void importPunchControlToTimeTrexDataBase(PunchControl[] users)throws Exception;  
}
