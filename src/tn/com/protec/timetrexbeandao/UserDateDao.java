/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.com.protec.timetrexbeandao;

import java.util.Date;
import java.util.List;
import tn.com.protec.timetrexbean.UserDate;

/**
 *
 * @author protec
 */
public interface UserDateDao {
    /**
	 * Insérer une nouvelle Ligne dans la table User.
	 * @param bean
     * @throws Exception
	 */
	public void insert(UserDate bean) throws Exception;
	/**
	 * Mettre à jour une ligne dans la table User.
	 * @param bean
     * @param IdUserDate
     * @throws Exception
     */
    public void update(UserDate bean) throws Exception;
	/**
	 * Supprimer une ligne de la table User.
	 * @param IdUserDate
     * @throws Exception
	 */
    public void delete(UserDate bean) throws Exception;
	/**
	 * Recherche par Clé Primaire
	 */
    public UserDate findByPrimaryKey(int idUserDate) throws Exception ;
    
    /**
	 * Recherche par datestamp
	 */
    public UserDate findByDateStamp(Date dateStamp) throws Exception ;
    
	/**
     * findAll
     * @return
     * @throws Exception
     */
    public List<UserDate>findAll() throws Exception;
            
     /*
     * 
     * Get UserDate from external DataBase 
     * To TimeTrex Data Base
     * 
     */
    public List<UserDate> findAllfromExternalDataBaseServerTable(String dataBase, String table) throws Exception;
    /*
     * 
     * Import UserDate to PostgreSQL
     * TimeTrex Data Base
     * */
    public void importUserDateToTimeTrexDataBase(UserDate[] punch)throws Exception;
}
