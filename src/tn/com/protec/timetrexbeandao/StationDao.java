package tn.com.protec.timetrexbeandao;

import java.util.List;

import tn.com.protec.timetrexbean.Station;

public interface StationDao {

    /**
     * Insérer une nouvelle Ligne dans la table User.
     *
     * @param bean
     * @throws Exception
     */
    public void insert(Station bean) throws Exception;

    /**
     * Mettre à jour une ligne dans la table User.
     *
     * @param bean
     * @param IdStation
     * @throws Exception
     */
    public void update(Station bean) throws Exception;

    /**
     * Supprimer une ligne de la table User.
     *
     * @param IdStation
     * @throws Exception
     */
    public void delete(Station bean) throws Exception;

    /**
     * Recherche par Clé Primaire
     */
    public Station findByPrimaryKey(int idStation) throws Exception;

    /**
     * Recherche par Address
     */
    public Station findByAddress(String source) throws Exception;

    /**
     * findAll
     *
     * @return
     * @throws Exception
     */
    public List<Station> findAll() throws Exception;
    
    /*
     * 
     * Get Station from external DataBase 
     * To TimeTrex Data Base
     * 
     */
    public List<Station> findAllfromExternalDataBaseServerTable() throws Exception;
    /*
     * 
     * Import Station to PostgreSQL
     * TimeTrex Data Base
     * */

    public void importStationToTimeTrexDataBase(Station[] Station) throws Exception;
}
