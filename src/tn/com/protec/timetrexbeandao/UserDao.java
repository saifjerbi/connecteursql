package tn.com.protec.timetrexbeandao;

import java.util.List;
import tn.com.protec.timetrexbean.Users;

public interface UserDao {
    /**
	 * Insérer une nouvelle Ligne dans la table User.
	 * @param bean
     * @throws Exception
	 */
	public void insert(Users bean) throws Exception;
	/**
	 * Mettre à jour une ligne dans la table User.
	 * @param bean
     * @param IdUser
     * @throws Exception
     */
    public void update(Users bean) throws Exception;
	/**
	 * Supprimer une ligne de la table User.
	 * @param IdUser
     * @throws Exception
	 */
    public void delete(Users bean) throws Exception;
	/**
	 * Recherche par username
	 */
    public Users findByname(String username ) throws Exception ;
	/**
	 * Recherche par Clé Primaire
	 */
    public Users findByPrimaryKey(int idUsers) throws Exception ;
    
	/**
     * findAll
     * @return
     * @throws Exception
     */
    public List<Users>findAll() throws Exception;
    
    /*
     * 
     * Get User from external DataBase 
     * To TimeTrex Data Base
     * 
     */
    public List<Users> findAllfromExternalDataBaseServerTable() throws Exception;
    /*
     * 
     * Import Users to PostgreSQL
     * TimeTrex Data Base
     * */
    public void importUserToTimeTrexDataBase(Users[] users)throws Exception;
}
