/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.com.protec.connection;

import java.util.List;
import tn.com.protec.timetrexbean.Punch;
import tn.com.protec.timetrexbean.Station;
import tn.com.protec.timetrexbean.Users;
import tn.com.protec.timetrexbeanDaoImpl.PunchDaoImpl;
import tn.com.protec.timetrexbeanDaoImpl.StationDaoImpl;
import tn.com.protec.timetrexbeanDaoImpl.UserDaoImpl;
import tn.com.protec.timetrexbeandao.PunchDao;
import tn.com.protec.timetrexbeandao.StationDao;
import tn.com.protec.timetrexbeandao.UserDao;

/**
 *
 * @author Jerbi
 */
public class ImportDataImpl {

    public static void importPunchToTimeTrex() throws Exception {
        PunchDao punchDao = new PunchDaoImpl();
        /*
         * get all punchs from external data base passed on Xml-db-Config File
         */
        long t1 = System.currentTimeMillis();
        List<Punch> punchs = punchDao.findAllfromExternalDataBaseServerTable();

        /*
         * Save all imported punchs to TimeTrex Data Base row by row
         */
        for (int i = 0; i < punchs.size(); i++) {
            punchDao.insert(punchs.get(i));
        }
        long t2 = System.currentTimeMillis();
        System.out.println("Import Punchs completed in " + (t2 - t1) + " ms)");
    }

    public static void importStationToTimeTrex() throws Exception {
        StationDao stationDao = new StationDaoImpl();
        /*
         * get all Stations from external data base passed on Xml-db-Config File
         */
        long t1 = System.currentTimeMillis();
        List<Station> stations = stationDao.findAllfromExternalDataBaseServerTable();
        /*
         * Save all imported Stations to TimeTrex Data Base row by row
         */
        for (int i = 0; i < stations.size(); i++) {
            stationDao.insert(stations.get(i));
        }
        long t2 = System.currentTimeMillis();
        System.out.println("Import Stations completed in " + (t2 - t1) + " ms)");

    }
    
    public static void importUsersToTimeTrex() throws Exception{
        UserDao userDao = new UserDaoImpl();
        /*
         * get all Users from external data base passed on Xml-db-Config File
         */
        long t1 = System.currentTimeMillis();
        List<Users> users = userDao.findAllfromExternalDataBaseServerTable();
        /*
         * Save all imported Users to TimeTrex Data Base row by row
         */
        for (int i = 0; i < users.size(); i++) {
            userDao.insert(users.get(i));
        }
        long t2 = System.currentTimeMillis();
        System.out.println("Import Users completed in " + (t2 - t1) + " ms)");
    }
}
