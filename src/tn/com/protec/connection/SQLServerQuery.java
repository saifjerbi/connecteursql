package tn.com.protec.connection;

import java.util.List;

import tn.com.protec.timetrexbean.Punch;
import tn.com.protec.timetrexbeanDaoImpl.PunchDaoImpl;
import tn.com.protec.timetrexbeandao.PunchDao;

public class SQLServerQuery {

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        // TODO Auto-generated method stub

        ImportDataImpl.importUsersToTimeTrex();
        ImportDataImpl.importStationToTimeTrex();
        ImportDataImpl.importPunchToTimeTrex();

    }
}
