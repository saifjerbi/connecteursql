/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.com.protec.timetrexbeanDaoImpl;

import java.util.List;
import tn.com.protec.abstractClass.AbstractFacade;
import tn.com.protec.timetrexbean.PunchControl;
import tn.com.protec.timetrexbeandao.PunchControlDao;

/**
 *
 * @author protec
 */
public class PunchControlDaoImpl extends AbstractFacade<PunchControl> implements PunchControlDao{
    public PunchControlDaoImpl() {
        super(PunchControl.class);
    }

    @Override
    public List<PunchControl> findAllfromExternalDataBaseServerTable(String dataBase, String table) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importPunchControlToTimeTrexDataBase(PunchControl[] users) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
