/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.com.protec.timetrexbeanDaoImpl;

import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import tn.com.protec.abstractClass.AbstractFacade;
import tn.com.protec.abstractClass.HibernateUtil;
import tn.com.protec.timetrexbean.UserDate;
import tn.com.protec.timetrexbeandao.UserDateDao;

/**
 *
 * @author protec
 */
public class UserDateDaoImpl extends AbstractFacade<UserDate> implements UserDateDao {

    public UserDateDaoImpl() {
        super(UserDate.class);
    }

    @Override
    public List<UserDate> findAllfromExternalDataBaseServerTable(String dataBase, String table) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void importUserDateToTimeTrexDataBase(UserDate[] punch) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public UserDate findByDateStamp(Date dateStamp) throws Exception {
        Session session = null;
        try {
            session = HibernateUtil.currentSession();
            Query query = session.createQuery("select p from UserDate as p where p.dateStamp=:dateStamp");
            query.setDate("dateStamp", dateStamp);
            UserDate userDate = (UserDate) query.uniqueResult();
            return userDate;

        } catch (Exception ex) {
            ex.printStackTrace();
            HibernateUtil.closeSession();
            return null;
        } finally {
            HibernateUtil.closeSession();
        }
    }
}
