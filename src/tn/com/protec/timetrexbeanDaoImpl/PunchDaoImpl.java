/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tn.com.protec.timetrexbeanDaoImpl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import tn.com.protec.abstractClass.AbstractFacade;
import tn.com.protec.dbSQLServerConnection.ConnectionFactorySQLServer;
import tn.com.protec.dbconfigFileParser.XmlDbTablesParameters;
import tn.com.protec.timetrexbean.Punch;
import tn.com.protec.timetrexbean.PunchControl;
import tn.com.protec.timetrexbean.Station;
import tn.com.protec.timetrexbean.UserDate;
import tn.com.protec.timetrexbean.Users;
import tn.com.protec.timetrexbeandao.PunchControlDao;
import tn.com.protec.timetrexbeandao.PunchDao;
import tn.com.protec.timetrexbeandao.StationDao;
import tn.com.protec.timetrexbeandao.UserDao;
import tn.com.protec.timetrexbeandao.UserDateDao;

/**
 *
 * @author Jerbi
 */
public class PunchDaoImpl extends AbstractFacade<Punch> implements PunchDao {

    public PunchDaoImpl() {
        super(Punch.class);
    }

    @Override
    public List<Punch> findAllfromExternalDataBaseServerTable() throws Exception {
        Properties props = XmlDbTablesParameters.getTablePropertieValue("./config-db-Punch-table.xml");
        Connection conn = null;
        PreparedStatement stmtSQLServer = null;
        ResultSet rsSQLServer = null;
        conn = ConnectionFactorySQLServer.getInstance(props.getProperty("database")).getConnection();
        
        final String sqlQuery = "SELECT * FROM "+props.getProperty("tablename")+" "
      + "WHERE ("+props.getProperty("statusId")+" = "+props.getProperty("statusIdIN")+") "
      + "OR ("+props.getProperty("statusId")+" = "+props.getProperty("statusIdOUT")+")";
        System.out.println(sqlQuery);
        System.out.println(sqlQuery);
        
        
        try {
            
//            stmtSQLServer = conn.prepareStatement("SELECT * FROM "+table+" WHERE EventType = 20");
            /*
             * on peut effectuer la requette suivante pour
             * Selectionner les pointages d'entrée
             * ou les pointage de sortie avec statusIdOUT
             * sinon on travail avec le EventType = 20 :accés autorisé - badge uniquement 
             * il existe dans la base avec le numero de badge et l'UserID
             * 
             */
            stmtSQLServer = conn.prepareStatement(sqlQuery);
            
            rsSQLServer = stmtSQLServer.executeQuery();
            UserDao userDao = new UserDaoImpl();
            UserDateDao userDateDao = new UserDateDaoImpl();
            StationDao stationDao = new StationDaoImpl();
            PunchControlDao punchControlDao = new PunchControlDaoImpl();
            List<Punch> temp = new ArrayList();
            int i;
            
            
            while (rsSQLServer.next()) {
//                if ((rsSQLServer.getShort("EventSubType") == 30)||(rsSQLServer.getShort("EventSubType") == 31)){
//                if (rsSQLServer.getShort("EventType") == 20){
                Punch punch = new Punch();
                PunchControl punchControl = new PunchControl();
                Users user = new Users();

//                i = (int) (rsSQLServer.getTimestamp("EventTime").getTime() / 1000);
                i = (int) (rsSQLServer.getTimestamp(props.getProperty("timestamp")).getTime() / 1000);

                //               String dateAsText = new SimpleDateFormat("yyyy-MM-dd")
//                        .format(new Date(rsSQLServer.getDate("EventTime").getTime()));
                
                /*
                 *       Si le dernier Punch est IN &&  le nouveau Punch est OUT
                 *       Alors {
                 *              1- Ajouter le nouveau Punch OUT avec le punchControlID est celui du dernier PunchIN
                 *              2- Calculer le temps passé en travail et {
                 *                                                          PunchControl.TotalTime <-- Temps calculé
                 *                                                          PunchControl.ActualTime <-- Temps calculé
                 *                                                          PunchControl.UpdatedDate <-- current date
                 *                                                          }
                 *              }
                 *      Sinon 1- Ajouter un nouveau PunchControl
                 *            2- Ajouter le nouveau Punch IN/OUT avec Punch.PunchControlID = PunchControl.ID crée
                 */
                
                /*
                 * Verifier le status du pointage
                 * Si le status = la valeur de pointage en entrée passée en fichier XML alor il prend la valeur 10 du timetrex systeme
                 * Sinon il prend la valeur de pointage en sortie de timetrex qui est égale à 20 parceque la requete SQL selection que les pointage entrée/sortie
                 * égale statusIdIN ou statusIdOUT passées dans le fichiers XML
                 */
                if (rsSQLServer.getInt(props.getProperty("statusId")) == rsSQLServer.getInt(props.getProperty("statusIdIN"))) {
                    punch.setStatusId(10);
                } else {
                    punch.setStatusId(20);
                }
                
                int punchControlID = createNewPunchControl(temp, rsSQLServer.getInt(props.getProperty("userId")), punch.getStatusId());
                /*
                 * il faut créer un nouveau punchControl
                 */
                if (punchControlID==-1) {
                    String dateAsText = new SimpleDateFormat("yyyy-MM-dd")
                            .format(new Date(rsSQLServer.getDate(props.getProperty("timestamp")).getTime()));
                    Date date = new SimpleDateFormat("yyyy-MM-dd").parse(dateAsText);

                    UserDate userDate = userDateDao.findByDateStamp(date);
                    if (userDate == null) {
                        /*
                         * if the User date does'nt exist on PuchContral Table 
                         * then
                         */
                        userDate = new UserDate();
                        createAndSaveUserDate(userDate, rsSQLServer, date, i, userDateDao);
                        System.out.println("userDate " + userDate.getId() + " " + userDate.getDateStamp() + " inserted");

                        punchControlID=createAndSavePunchControl(punchControl, userDate, i, punchControlDao);
                    } else {
                        punchControlID=createAndSavePunchControl(punchControl, userDate, i, punchControlDao);
                    }
                }
                else{
                    /*
                     * 2- Calculer le temps passé en travail et modifier{
                     *                                            PunchControl.TotalTime <-- Temps calculé
                     *                                            PunchControl.ActualTime <-- Temps calculé
                     *                                            PunchControl.UpdatedDate <-- current date
                     */
                    calculateTime(punchControlDao, punchControlID, i);
                }
                
                punch.setPunchControlId(punchControlID);
//                punch.setId(rsSQLServer.getInt("EventID"));
                punch.setId(rsSQLServer.getInt(props.getProperty("id")));
//                punch.setTimeStamp(rsSQLServer.getTimestamp("EventTime"));
                punch.setTimeStamp(rsSQLServer.getTimestamp(props.getProperty("timestamp")));

                /*
                 * Find Station Id by Events.Address
                 */
                Station station;
//                station = stationDao.findByAddress(rsSQLServer.getString("Address"));
                station = stationDao.findByAddress(rsSQLServer.getString(props.getProperty("address")));
                punch.setStationId(station.getId());
                punch.setCreatedBy(rsSQLServer.getInt(props.getProperty("userId")));
                punch.setCreatedDate(i);
                punch.setUpdatedBy(rsSQLServer.getInt(props.getProperty("userId")));
                punch.setUpdatedDate(i);
                
                
                punch.setTypeId(Integer.valueOf(props.getProperty("typeid")));/*Punch type : Normal(10) - Lunch(20) - Break(30) passed on Xml file*/


//                punch.setOriginalTimeStamp(rsSQLServer.getTimestamp("EventTime"));
                punch.setOriginalTimeStamp(rsSQLServer.getTimestamp(props.getProperty("timestamp")));
//                punch.setActualTimeStamp(rsSQLServer.getTimestamp("EventTime"));
                punch.setActualTimeStamp(rsSQLServer.getTimestamp(props.getProperty("timestamp")));
                punch.setLongitude(new BigDecimal(0.0000000000));
                punch.setLatitude(new BigDecimal(0.0000000000));
                punch.setTransfer((short) 0);
                temp.add(punch);

            }
            Punch ret[] = new Punch[temp.size()];

            System.out.println("Fetched " + ret.length + " rows.");
            return temp;

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            throw new Exception(sqlException.getMessage());
        } finally {
            if (rsSQLServer != null) {
                try {
                    rsSQLServer.close();
                } catch (Exception e) {
                }
            }
            if (stmtSQLServer != null) {
                try {
                    stmtSQLServer.close();
                } catch (Exception e) {
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                }
            }
        }
    }

    @Override
    public void importPunchToTimeTrexDataBase(Punch[] punch) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private int createAndSavePunchControl(PunchControl punchControl, UserDate userDate, int i, PunchControlDao punchControlDao) throws Exception {
        /*Punch control
         */

        punchControl.setUserDateId(userDate.getId());
        punchControl.setBranchId(0);
        punchControl.setDepartmentId(0);
        punchControl.setJobId(0);
        punchControl.setJobItemId(0);
        punchControl.setQuantity(new BigDecimal(0));
        punchControl.setBadQuantity(new BigDecimal(0));
        punchControl.setOtherId1("");
        punchControl.setOtherId2("");
        punchControl.setOtherId3("");
        punchControl.setOtherId4("");
        punchControl.setOtherId5("");
        punchControl.setCreatedBy(1);
        punchControl.setCreatedDate(i);
        punchControl.setUpdatedBy(1);
        punchControl.setUpdatedDate(i);
        punchControl.setNote("Imported Data");
        /*
         * save punchControl created
         */
        punchControlDao.insert(punchControl);
        System.out.println("punchControl " + punchControl.getId() + " inserted");
        return punchControl.getId();
    }

    private void createAndSaveUserDate(UserDate userDate, ResultSet rsSQLServer, Date date, int i, UserDateDao userDateDao) throws SQLException, ParseException, Exception {
        /*
         UserDate
         */
        userDate.setUserId(1);
        userDate.setPayPeriodId(6);
        userDate.setDateStamp(date);
        userDate.setDeleted((short) 0);
        userDate.setCreatedBy(1);
        userDate.setCreatedDate(i);
        userDate.setUpdatedBy(1);
        userDate.setUpdatedDate(i);
        
        /*
         * save userDate created
         */
        userDateDao.insert(userDate);
    }
    
    

    private int createNewPunchControl(List<Punch> temp, int userID, int punchStatus) {
        for (int i = temp.size(); i >= 0; i--) {
            if ((temp.get(i).getCreatedBy() == userID) && (temp.get(i).getStatusId() == 10)&&(punchStatus ==20)) {
                return temp.get(i).getPunchControlId();
            }
            else if(temp.get(i).getCreatedBy()==userID)
            {
                return -1;
            }
    }
        return -1;
    }

    private void calculateTime(PunchControlDao punchControlDao, int punchControlID, int i ) throws Exception {
        /*
         * Calculer le temps dépassé en travail pour ce punchControlID
         * et modifier UpdatedDate
         */
        PunchControl punchControl = punchControlDao.findByPrimaryKey(punchControlID);

        punchControl.setUpdatedDate(i);
        Date createdDate = new Date((long) punchControl.getCreatedDate());
        Date updatedDate = new Date((long) i);
        int timeexpired = (int) ((updatedDate.getTime() - createdDate.getTime()) / 1000);
        punchControl.setTotalTime(timeexpired);
        punchControl.setActualTotalTime(timeexpired);
        punchControlDao.update(punchControl);

    }
}
