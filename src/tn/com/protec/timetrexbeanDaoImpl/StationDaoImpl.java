package tn.com.protec.timetrexbeanDaoImpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.hibernate.Query;
import org.hibernate.Session;

import tn.com.protec.abstractClass.AbstractFacade;
import tn.com.protec.abstractClass.HibernateUtil;
import tn.com.protec.dbSQLServerConnection.ConnectionFactorySQLServer;
import tn.com.protec.dbconfigFileParser.XmlDbTablesParameters;
import tn.com.protec.timetrexbean.Station;
import tn.com.protec.timetrexbeandao.StationDao;

public class StationDaoImpl extends AbstractFacade<Station> implements StationDao {

    /**
     * @param args
     */
    public StationDaoImpl() {
        super(Station.class);
    }

    @Override
    public List<Station> findAllfromExternalDataBaseServerTable() throws Exception {
        Properties props = XmlDbTablesParameters.getTablePropertieValue("./config-db-Station-table.xml");
        Connection conn = null;
        PreparedStatement stmtSQLServer = null;
        ResultSet rsSQLServer = null;
        conn = ConnectionFactorySQLServer.getInstance(props.getProperty("database")).getConnection();
        final String sqlQuery = "SELECT * FROM "+props.getProperty("tablename");

        try {

            stmtSQLServer = conn.prepareStatement(sqlQuery);
            rsSQLServer = stmtSQLServer.executeQuery();

            List temp = new ArrayList();
            while (rsSQLServer.next()) {
                Station station = new Station();
//                station.setId(rsSQLServer.getInt("ID"));
                station.setId(rsSQLServer.getInt(props.getProperty("id")));
//                station.setStationId(rsSQLServer.getString("ID"));
                station.setStationId(rsSQLServer.getString(props.getProperty("id")));
//                station.setPort(rsSQLServer.getInt("CommPort"));
                station.setPort(rsSQLServer.getInt(props.getProperty("port")));
//                station.setTypeId(rsSQLServer.getInt("Type"));
                station.setTypeId(rsSQLServer.getInt(props.getProperty("type")));
//                station.setSource(rsSQLServer.getString("Address"));
                station.setSource(rsSQLServer.getString(props.getProperty("source")));
//                station.setModeFlag(rsSQLServer.getLong("StatusFlag"));
                station.setModeFlag(rsSQLServer.getLong(props.getProperty("modeFlag")));
//                station.setPollFrequency(rsSQLServer.getInt("PollingSlot"));
                station.setPollFrequency(rsSQLServer.getInt(props.getProperty("pollingFrequency")));
//                station.setDescription("Imported Device");
                station.setDescription(props.getProperty("description"));
//                station.setStationId("0");
                temp.add(station);
            }
            Station ret[] = new Station[temp.size()];

            System.out.println("Fetched " + ret.length + " rows.");
            return temp;

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            throw new Exception(sqlException.getMessage());
        } finally {
            if (rsSQLServer != null) {
                try {
                    rsSQLServer.close();
                } catch (Exception e) {
                }
            }
            if (stmtSQLServer != null) {
                try {
                    stmtSQLServer.close();
                } catch (Exception e) {
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                }
            }
        }

    }

    @Override
    public void importStationToTimeTrexDataBase(Station[] Station)
            throws Exception {
        // TODO Auto-generated method stub
    }

    @Override
    public Station findByAddress(String source) throws Exception {
        Session session = null;
        try {
            session = HibernateUtil.currentSession();
            Query query = session.createQuery("select p from Station as p where p.source=:source");
            query.setString("source", source);
            Station station = (Station) query.uniqueResult();
            return station;

        } catch (Exception ex) {
            ex.printStackTrace();
            HibernateUtil.closeSession();
            return null;
        } finally {
            HibernateUtil.closeSession();
        }

    }
}
