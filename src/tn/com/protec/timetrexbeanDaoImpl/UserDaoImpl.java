package tn.com.protec.timetrexbeanDaoImpl;

import tn.com.protec.timetrexbeandao.UserDao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.hibernate.Query;
import org.hibernate.Session;
import tn.com.protec.abstractClass.AbstractFacade;
import tn.com.protec.abstractClass.HibernateUtil;

import tn.com.protec.dbPostgresSQLConnection.ConnectFactoryPostgreSQL;
import tn.com.protec.dbSQLServerConnection.ConnectionFactorySQLServer;
import tn.com.protec.dbconfigFileParser.XmlDbTablesParameters;
import tn.com.protec.timetrexbean.Users;

public class UserDaoImpl extends AbstractFacade<Users> implements UserDao {

    public UserDaoImpl() {
        super(Users.class);
    }

    @Override
    public List<Users> findAllfromExternalDataBaseServerTable() throws Exception {
        // TODO Auto-generated method stub
        Properties props = XmlDbTablesParameters.getTablePropertieValue("./config-db-Users-table.xml");
        Connection conn = null;
        PreparedStatement stmtSQLServer = null;
        ResultSet rsSQLServer = null;
        conn = ConnectionFactorySQLServer.getInstance(props.getProperty("database")).getConnection();
        final String sqlQuery = "SELECT * FROM "+props.getProperty("tablename");

        
        try {

            stmtSQLServer = conn.prepareStatement(sqlQuery);
            rsSQLServer = stmtSQLServer.executeQuery();

            List temp = new ArrayList();
            while (rsSQLServer.next()) {
                Users user = new Users();
//                user.setId(rsSQLServer.getInt("UserID"));
                user.setId(rsSQLServer.getInt(props.getProperty("id")));
                user.setEmployeeNumber(rsSQLServer.getString(props.getProperty("id")));
                user.setCompanyId(1);
//                user.setUserName(rsSQLServer.getString("FirstName"));
                user.setUserName(rsSQLServer.getString(props.getProperty("userName")));
                user.setPassword("6149477f05ef126a6f07c3ba593637edadc96afe");
                user.setStatusId(10);
//                user.setFirstName(rsSQLServer.getString("FirstName"));
                user.setFirstName(rsSQLServer.getString(props.getProperty("firstName")));
//                user.setMiddleName(rsSQLServer.getString("MiddleName"));
                user.setMiddleName(rsSQLServer.getString(props.getProperty("middleName")));
//                user.setLastName(rsSQLServer.getString("Surname"));
                user.setLastName(rsSQLServer.getString(props.getProperty("lastName")));
//                user.setWorkPhone(rsSQLServer.getString("Telephone"));
                user.setWorkPhone(rsSQLServer.getString(props.getProperty("workPhone")));
//                user.setHomePhone(rsSQLServer.getString("Telephone"));
                user.setHomePhone(rsSQLServer.getString(props.getProperty("homePhone")));
//                user.setFaxPhone(rsSQLServer.getString("Fax"));
                user.setFaxPhone(rsSQLServer.getString(props.getProperty("faxPhone")));
//                user.setAddress1(rsSQLServer.getString("Field1_100"));
                user.setAddress1(rsSQLServer.getString(props.getProperty("address1")));
                temp.add(user);
            }
            Users ret[] = new Users[temp.size()];

            System.out.println("Fetched " + ret.length + " rows.");
            return temp;

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
            throw new Exception(sqlException.getMessage());
        } finally {
            if (rsSQLServer != null) {
                try {
                    rsSQLServer.close();
                } catch (Exception e) {
                }
            }
            if (stmtSQLServer != null) {
                try {
                    stmtSQLServer.close();
                } catch (Exception e) {
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                }
            }
        }
    }

    @Override
    public void importUserToTimeTrexDataBase(Users[] users) throws Exception {
        
    }

    @Override
    public Users findByname(String username) throws Exception {
        Session session = null;
        try {
            session = HibernateUtil.currentSession();
            Query query = session.createQuery("select p from Users as p where p.userName=:username");
            query.setString("username", username);
            Users users = (Users) query.uniqueResult();
            return users;

        } catch (Exception ex) {
            ex.printStackTrace();
            HibernateUtil.closeSession();
            return null;
        } finally {
            HibernateUtil.closeSession();
        }

    }
}
