﻿//The newFeedback variable controls the feedback look and feel.
newFeedback = true;
//These values will be replaced by the build.
L_alias = "sqldocfb@microsoft.com";
L_product = "JDBC";
L_deliverable = "JDBC";
L_docversion = "RTW";
L_productversion = "3.0.10090.0126";
//End of values replaced by the build.
L_fdintro = "Ces informations vous ont-elles été utiles ?";
L_fdwhywrong = "Pourquoi ?";
L_fdwhatwrong = "Que recherchiez-vous ? (750 caractères maximum)";
L_fdinfowrong = "Ces informations ne sont pas correctes";
L_fdneedsmore = "Ces informations ne sont pas assez précises";
L_fdnotexpected = "Ces informations ne correspondent pas à mes attentes";
L_fdyes = "Oui";
L_fdno = "Non";
L_fdback = "Précédent";
L_fdnext = "Suivant";
L_fdsubmit = "Envoyer";
L_fdaltyes = "Oui";
L_fdaltno = "Non";
L_fdaltback = "Précédent";
L_fdaltnext = "Suivant";
L_fdaltsubmit = "Envoyer";
L_fddefaultbody = "";
//NEW FEEDBACK SYSTEM
L_FeedBackDivID = "fb";
L_fbintroduction = "Microsoft apprécie vos commentaires. Pour envoyer vos commentaires sur cette rubrique à l'équipe de documentation, cliquez sur <b>Envoyer des commentaires</b> et tapez vos commentaires dans le message électronique avant de l'envoyer à Microsoft. Pour obtenir de l'aide sur les problèmes de prise en charge, consultez les informations de support technique fournies avec le produit.";
L_fbsend = "Envoyer des commentaires";
L_fbaltsend = "Envoyer des commentaires";
L_fb1Poor = "Médiocre";
L_fb1Excellent = "Non résolu";
L_fb1EnterFeedbackHere_Text = "Pour envoyer vos commentaires par courrier électronique à Microsoft, cliquez ici :";
L_fb1Title_Text = "Commentaires sur la documentation";
//L_sTechSupportHREF="<MSHelp:link keywords=\"be69811b-5b47-46fe-8fc9-c5d6e4a13868\" TABINDEX=\"0\">Analysis Services Architecture (Analysis Services)</MSHelp:link>";
L_fb1Acknowledge="Merci";
L_fbaltIcon = "Afficher les instructions relatives aux commentaires au bas de la page."
//Search fix (sf)
L_sf_langF="Filtre de langage";
L_sf_all="Tous";
L_sf_multiple="Multiple";
L_sf_LangFilter="Filtre de langage";
L_sf_All="Tous";
L_sf_Multiple="Multiple";
L_sf_Declaration="Déclaration";
L_sf_Usage="Utilisation";
L_sf_VB="Visual Basic";
L_sf_CSharp="C#";
L_sf_CPlus="C++";
L_sf_JSharp="J#";
L_sf_JScript="JScript";
L_sf_Xml="Xml";
L_sf_Html="Html";
L_sf_CollAl="Réduire tout";
L_sf_ExpAll="Développer tout";
L_sf_Roles="Rôles pour cette rubrique…";
L_sf_Info="Centres d'informations";
L_sf_MemAll="Options des membres : Afficher tout";
L_sf_MemFilt="Options des membres : Filtré";
L_sf_MemInh="Inclure les membres hérités";
L_sf_MemPro="Inclure les membres protégés";
L_sf_MemCom="Membres du .NET Compact Framework uniquement";
