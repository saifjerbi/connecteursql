Informations sur la version
Pilote JDBC Microsoft�SQL�Server�3.0 
Avril 2010


INTRODUCTION
------------
Le pr�sent fichier contient des informations importantes ou de derni�re minute qui 
compl�tent la documentation du pilote JDBC Microsoft SQL Server. Il est recommand� 
de lire ce fichier enti�rement avant de proc�der � l'installation du pilote JDBC.

Nous attachons une grande importance � vos commentaires et nous efforcerons 
de les prendre en compte le plus rapidement possible. Pour plus d'informations 
sur l'envoi de commentaires via le groupe de discussion relatif au pilote JDBC 
et les forums en ligne, visitez la page consacr�e au pilote JDBC de Microsoft SQL Server 
� l'adresse http://msdn.microsoft.com/data/jdbc.


INSTALLATION
------------
Les instructions relatives � l'installation du pilote JDBC se trouvent dans le fichier install.txt.
R�f�rez-vous � ce fichier pour plus d'informations sur l'installation du 
pilote JDBC sur les syst�mes d'exploitation Windows et Unix.

SYST�MES D'EXPLOITATION PRIS EN CHARGE
---------------------------
Le pilote JDBC Microsoft SQL�Server�3.0 prend en charge les syst�mes d'exploitation suivants�: 
Linux, Unix, Windows Server�2003 Service Pack�2, Windows Server�2008, 
Windows Vista Service Pack�1, Windows�XP Service Pack�3, 
Windows Server�2008�R2 et Windows�7.

La liste ci-dessus pr�sente certains des syst�mes d'exploitation pris en charge.
Le pilote JDBC est con�u pour fonctionner sur tout syst�me d'exploitation prenant 
en charge l'utilisation d'une machine virtuelle Java (JVM). Toutefois, seuls les 
syst�mes d'exploitation Sun Solaris, SUSE Linux et Windows�XP (ou versions ult�rieures) 
ont �t� test�s.

CONTENU DE LA VERSION
----------------
Le fichier ex�cutable zip ou tar du pilote JDBC Microsoft SQL Server�d�compresse 
les fichiers suivants aux emplacements sp�cifi�s, relatifs au r�pertoire 
d'installation s�lectionn�:

<installation directory>\sqljdbc_<version>\<language>\install.txt
<installation directory>\sqljdbc_<version>\<language>\release.txt
<installation directory>\sqljdbc_<version>\<language>\license.txt
<installation directory>\sqljdbc_<version>\<language>\sqljdbc.jar
<installation directory>\sqljdbc_<version>\<language>\sqljdbc4.jar
<installation directory>\sqljdbc_<version>\<language>\auth\x86\sqljdbc_auth.dll
<installation directory>\sqljdbc_<version>\<language>\auth\x64\sqljdbc_auth.dll
<installation directory>\sqljdbc_<version>\<language>\auth\ia64\sqljdbc_auth.dll
<installation directory>\sqljdbc_<version>\<language>\help\default.htm
<installation directory>\sqljdbc_<version>\<language>\help\index.htm
<installation directory>\sqljdbc_<version>\<language>\help\toc.htm
<installation directory>\sqljdbc_<version>\<language>\help\html\<doc pages...>
<installation directory>\sqljdbc_<version>\<language>\help\local\<doc files...>
<installation directory>\sqljdbc_<version>\<language>\help\samples\<sample files...>
<installation directory>\sqljdbc_<version>\<language>\xa\xa_install.sql
<installation directory>\sqljdbc_<version>\<language>\xa\x86\sqljdbc_xa.dll
<installation directory>\sqljdbc_<version>\<language>\xa\x64\sqljdbc_xa.dll
<installation directory>\sqljdbc_<version>\<language>\xa\ia64\sqljdbc_xa.dll


LISTE DE MODIFICATIONS
-----------
Modifications de 2.0 � 3.0�:

327029 L'exception ���chec du chargement de sqljdbc_auth.dll�� contient maintenant 
       des informations suppl�mentaires sur la cause de l'�chec.

329180 Les serveurs d'applications peuvent d�sormais utiliser l'interface Wrapper pour impl�menter 
       les proxys des instances de SQLServerDataSource et d'autres classes impl�mentant l'interface Wrapper.

293533 La valeur de retour d'une proc�dure stock�e ex�cut�e par un d�clencheur de table n'a plus 
       d'incidence sur le nombre de mises � jour d'une requ�te INSERT, UPDATE ou DELETE, 
       lorsque lastUpdateCount=true

345336 Le probl�me de boucle infinie dans Statement.close() apr�s la fermeture inattendue de sa 
       connexion sous-jacente a �t� r�solu.

351393 executeUpdate retourne � pr�sent le nombre correct de mises � jour pour une requ�te BULK INSERT.

327052 ResultSet.getHoldability() retourne � pr�sent ResultSet.HOLD_CURSORS_OVER_COMMIT lorsqu'une 
       conversion de curseur implicite produit un jeu de r�sultats qui n'est pas soutenu par un 
       curseur c�t� serveur.

374320 Le probl�me de boucle infinie dans ResultSet.close() lorsque la m�thode rencontre un 
       d�passement de d�lai d'attente de verrou ou une autre erreur de ligne a �t� r�solu.

373923 Le pilote utilise maintenant le protocole TLS pour ses communications SSL, � des fins 
       de conformit� avec les normes FIPS (Federal Information Processing Standard).

376322 Le pilote se connecte maintenant � un serveur partenaire de basculement faisant office 
       de principal dans un environnement Kerberos.

305612 ResultSet.getInt("DATA_TYPE") ne l�ve plus de NullPointerException avec le type TABLE SQL.

411695 getSchemas doit retourner des informations de sch�ma. Dans les versions ant�rieures du pilote, 
       celui-ci retournait des informations d'utilisateur au lieu des informations de sch�ma. 
       Le pilote retourne maintenant des informations de sch�ma et prend en charge les crit�res 
       sp�ciaux de nom de sch�ma par sp�cification JDBC.


PROBL�MES CONNUS
------------
Voici une liste des probl�mes connus li�s au pilote JDBC Microsoft SQL Server�3.0�:


1) LIMITATIONS DES M�TADONN�ES DE PARAM�TRE AVEC L'INSTRUCTION SQL MERGE

PreparedStatement.getParameterMetadata() l�ve SQLException en cas d'utilisation avec 
une requ�te MERGE param�trable.

